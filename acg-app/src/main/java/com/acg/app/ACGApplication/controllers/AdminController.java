package com.acg.app.ACGApplication.controllers;

import com.acg.app.ACGApplication.commands.AuthenticateCommand;
import com.acg.app.ACGApplication.services.AdminService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/admin")
public class AdminController {

    private final AdminService adminService;
    private static final Logger logger = LogManager.getLogger(AdminController.class);

    @Autowired
    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @PostMapping("/auth")
    public boolean authenticate(@RequestBody AuthenticateCommand command){
        logger.info("Admin authentication");
        return adminService.authenticate(command.getUsername(), command.getPassword());
    }
}
