package com.acg.app.ACGApplication.services;

public interface AdminService {

    boolean authenticate(String username, String password);

    Integer getExchangeRate();
}
