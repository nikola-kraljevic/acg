import React, {useContext} from 'react'
import {useRoutes} from "hookrouter";
import NotFound from "../../Errors/NotFound/NotFound";
import AdminCtx from "../../../contexts/AdminCtx";
import NotAuthenticated from "../../Errors/NotAuthenticated/NotAuthenticated";
import EditCar from "./EditCar/EditCar";
import Cars from "../UserScreen/Cars/Cars";
import NewCarForm from "./NewCarForm/NewCarForm";
import NewCarViaUrl from "./NewCarViaUrl/NewCarViaUrl";

const AdminRoutingHoc:React.FC = () => {

    const adminCtx = useContext(AdminCtx)

    const routes = {
        '/': () => <Cars/>,
        '/new': () => <NewCarForm car={{
            price:undefined,
            location:undefined,
            make:undefined,
            model:undefined,
            type:undefined,
            productionYear:undefined,
            modelYear:undefined,
            mileage:undefined,
            motor:undefined,
            motorPower:undefined,
            motorCubicCentimeters:undefined,
            gearbox:undefined,
            numberOfGears:undefined,
            description:undefined,
            used:undefined,
            hasServiceBook:undefined,
            garaged:undefined,
        }}/>,
        '/new/url': () => <NewCarViaUrl/>,
        '/edit/:id': ({id}: any) => <EditCar id={id}/>
    }

    const routeResult = useRoutes(routes)

    return(
        adminCtx.authenticated ? (
            <>
                { routeResult || <NotFound/> }
            </>
        ) : (
            <>
                <NotAuthenticated/>
            </>
        )
    );
}

export default AdminRoutingHoc