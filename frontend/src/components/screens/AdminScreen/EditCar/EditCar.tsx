import React, {useEffect, useState} from "react";
import {Car} from "../../../../types/Car";
import {Alert, Button, Input, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {Formik} from "formik";
import {
    changeProfilePicture,
    deleteCarById,
    deleteImageById,
    editCar, featureCarById,
    getCarByCarId,
    getCarImageIdListByCarId,
    sellCarById, unFeatureCarById, unsellCarById
} from "../../../../api";
import {IoIosCloseCircleOutline, IoIosPerson} from 'react-icons/io'
import {navigate} from "hookrouter";

type Props = {id:string}

const EditCar:React.FC<Props> = (props) => {

    const [depsTrigger, setDepsTrigger] = useState(0)
    const [modal, setModal] = useState(false);
    const [car, setCar] = useState<Car | null>(null)
    const [desc, setDesc] = useState()
    const [pictures, setPictures] = useState<File[]>([])
    const [images, setImages] = useState<{id:string}[]>([])

    const toggle = () => setModal(!modal);

    function onFileChangeHandler(e:any){
        setPictures(e.target.files)
    }

    function handleDescriptionChange(e: any) {
        setDesc(e.target.value)
    }

    function handleCarEdit(val:Car,bag:any){
        bag.setSubmitting(true)
        editCar(val).then((response) => {
            console.log(response)
            bag.setSubmitting(false)
        })
    }

    function handleDeleteCar(id:string){
        deleteCarById(id).then(response => {
            navigate('/auti')
        })
    }

    function handleDeleteImage(id:string){
        deleteImageById(id).then(resp => console.log(resp))
        setDepsTrigger(depsTrigger+1)
    }

    function handleSellCar(id:string){
        sellCarById(id).then(response => {
            if(car){
                setCar({...car,sold:true})
            }
        })
    }

    function handleUnsellCar(id:string){
        unsellCarById(id).then(response => {
            if(car){
                setCar({...car,sold:false})
            }
        })
    }

    function handleCarFeature(id:string) {
        featureCarById(id).then(response => {
            if(car){
                setCar({...car,featured:true})
            }
        })
    }

    function handleCarUnFeature(id:string) {
        unFeatureCarById(id).then(response => {
            if(car){
                setCar({...car,featured:false})
            }
        })
    }

    function changeProfilePic(carId:string,imageId:string){
        changeProfilePicture(carId,imageId).then(response => console.log(response))
        setDepsTrigger(depsTrigger+1)
    }

    useEffect(() => {
        getCarByCarId(props.id).then((response) => {
            setCar(response)
            setDesc(response.description)
        })
        getCarImageIdListByCarId(props.id)
            .then(response => setImages(response.data))
            .catch(err => console.log('error while getting images:' + err.toString()))
    },[depsTrigger])

    return(
        <>
            <div>
                {car? (
                    <>
                        {!car.sold && <Button type="button" color="info" onClick={() => (handleSellCar(car.id))}>Sell</Button>}
                        {car.sold && <Button type="button" color="info" onClick={() => (handleUnsellCar(car.id))}>UnSell</Button>}
                        {!car.featured && <Button type="button" color="warning" onClick={() => (handleCarFeature(car.id))}>Feature</Button>}
                        {car.featured && <Button type="button" color="warning" onClick={() => (handleCarUnFeature(car.id))}>UnFeature</Button>}
                        <Button color="danger" onClick={() => toggle()}>Delete</Button>
                        <Formik
                            initialValues={car}
                            onSubmit={(values,{setSubmitting})=>{handleCarEdit(values,{setSubmitting})}}
                            render={({
                                         values,
                                         errors,
                                         touched,
                                         handleChange,
                                         handleBlur,
                                         handleSubmit,
                                         isSubmitting,
                                     }) => (
                                <form onSubmit={handleSubmit} className="newCarForm">
                                    <ul>
                                        <li>
                                            <label>Cijena:</label>
                                            <input
                                                type="number"
                                                name="price"
                                                placeholder="price"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.price}
                                            />
                                        </li>{errors.price && touched.price && <Alert color="danger">{errors.price}</Alert>}
                                        <li>
                                            <label>Lokacija vozila:</label>
                                            <input
                                                type="text"
                                                name="location"
                                                placeholder="location"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.location}
                                            />
                                        </li>{errors.location && touched.location && <Alert color="danger">{errors.location}</Alert>}
                                        <li>
                                            <label>Marka automobila:</label>
                                            <input
                                                type="text"
                                                name="make"
                                                placeholder="make"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.make}
                                            />
                                        </li>{errors.make && touched.make && <Alert color="danger">{errors.make}</Alert>}
                                        <li>
                                            <label>Model automobila:</label>
                                            <input
                                                type="text"
                                                name="model"
                                                placeholder="model"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.model}
                                            />
                                        </li>{errors.model && touched.model && <Alert color="danger">{errors.model}</Alert>}
                                        <li>
                                            <label>Tip automobila:</label>
                                            <input
                                                type="text"
                                                name="type"
                                                placeholder="type"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.type}
                                            />
                                        </li>{errors.type && touched.type && <Alert color="danger">{errors.type}</Alert>}
                                        <li>
                                            <label>Godina proizvodnje:</label>
                                            <input
                                                type="number"
                                                name="productionYear"
                                                placeholder="productionYear"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.productionYear}
                                            />
                                        </li>{errors.productionYear && touched.productionYear && <Alert color="danger">{errors.productionYear}</Alert>}
                                        <li>
                                            <label>Godina modela:</label>
                                            <input
                                                type="number"
                                                name="modelYear"
                                                placeholder="modelYear"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.modelYear}
                                            />
                                        </li>{errors.modelYear && touched.modelYear && <Alert color="danger">{errors.modelYear}</Alert>}
                                        <li>
                                            <label>Prijeđeni kilometri:</label>
                                            <input
                                                type="number"
                                                name="mileage"
                                                placeholder="mileage"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.mileage}
                                            />
                                        </li>{errors.mileage && touched.mileage && <Alert color="danger">{errors.mileage}</Alert>}
                                        <li>
                                            <label>Motor:</label>
                                            <input
                                                type="text"
                                                name="motor"
                                                placeholder="motor"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.motor}
                                            />
                                        </li>{errors.motor && touched.motor && <Alert color="danger">{errors.motor}</Alert>}
                                        <li>
                                            <label>Snaga motora:</label>
                                            <input
                                                type="number"
                                                name="motorPower"
                                                placeholder="motorPower"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.motorPower}
                                            />
                                        </li>{errors.motorPower && touched.motorPower && <Alert color="danger">{errors.motorPower}</Alert>}
                                        <li>
                                            <label>Radni obujam:</label>
                                            <input
                                                type="number"
                                                name="motorCubicCentimeters"
                                                placeholder="motorCubicCentimeters"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.motorCubicCentimeters}
                                            />
                                        </li>{errors.motorCubicCentimeters && touched.motorCubicCentimeters && <Alert color="danger">{errors.motorCubicCentimeters}</Alert>}
                                        <li>
                                            <label>Mjenjač:</label>
                                            <input
                                                type="text"
                                                name="gearbox"
                                                placeholder="gearbox"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.gearbox}
                                            />
                                        </li>{errors.gearbox && touched.gearbox && <Alert color="danger">{errors.gearbox}</Alert>}
                                        <li>
                                            <label>Broj stupnjeva:</label>
                                            <input
                                                type="number"
                                                name="numberOfGears"
                                                placeholder="numberOfGears"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.numberOfGears}
                                            />
                                        </li>{errors.numberOfGears && touched.numberOfGears && <Alert color="danger">{errors.numberOfGears}</Alert>}
                                        <li>
                                            <label>Rabljeno:</label>
                                            <input
                                                type="checkbox"
                                                name="used"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                checked={values.used}
                                            />
                                        </li>{errors.used && touched.used && <Alert color="danger">{errors.used}</Alert>}
                                        <li>
                                            <label>Servisna knjiga:</label>
                                            <input
                                                type="checkbox"
                                                name="hasServiceBook"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                checked={values.hasServiceBook}
                                            />
                                        </li>{errors.hasServiceBook && touched.hasServiceBook && <Alert color="danger">{errors.hasServiceBook}</Alert>}
                                        <li>
                                            <label>Garažiran:</label>
                                            <input
                                                type="checkbox"
                                                name="garaged"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                checked={values.garaged}
                                            />
                                        </li>{errors.garaged && touched.garaged && <Alert color="danger">{errors.garaged}</Alert>}
                                        <li>
                                            <label>images:</label>
                                            <input type="file" className="form-control" name="file" multiple onChange={onFileChangeHandler}/>
                                        </li>
                                        <li>
                                            <Input rows={10} type="textarea" name="text" id="exampleText" onChange={handleDescriptionChange} value={desc}/>
                                        </li>
                                        <li>
                                            <Button color="success" type="submit" disabled={isSubmitting}>
                                                Edit
                                            </Button>
                                        </li>
                                    </ul>
                                </form>
                            )}
                        />
                    </>
                ) : (
                    <>
                        Loading...
                    </>
                )}
            </div>
            <div>
                {images&&car? (
                    <>
                        {images.map((image) =>
                            {
                                if(image.id != car.profilePicture){
                                    return(
                                        <div className="adminImage" style={{backgroundImage:"url("+process.env.REACT_APP_API_URL+"/images/"+ image.id +")"}}>
                                            <IoIosCloseCircleOutline
                                                onClick={() => handleDeleteImage(image.id)}
                                                className="adminImageIco"
                                                size="2.5em"
                                                style={{float:"right"}}
                                            />
                                            <IoIosPerson
                                                onClick={()=>changeProfilePic(car.id,image.id)}
                                                size="2.5em"
                                                style={{float:"left",color:"Blue"}}
                                            />
                                        </div>
                                    )
                                } else if (image.id == car.profilePicture){
                                    return (
                                        <div className="adminImage" style={{backgroundImage:"url("+process.env.REACT_APP_API_URL+"/images/"+ image.id +")"}}>
                                            <IoIosCloseCircleOutline
                                                onClick={() => handleDeleteImage(image.id)}
                                                className="adminImageIco"
                                                size="2.5em"
                                                style={{float:"right"}}
                                            />
                                            <IoIosPerson
                                                size="2.5em"
                                                style={{float:"left",color:"Yellow"}}
                                            />
                                        </div>
                                    )
                                }
                            }
                        )}
                    </>
                ):(
                    <>
                        Loading...
                    </>
                )}
            </div>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>WARNING!</ModalHeader>
                <ModalBody>
                    Are you sure you want to delete this car?
                </ModalBody>
                <ModalFooter>
                    <Button color="danger" onClick={()=>handleDeleteCar(props.id)}>Delete</Button>{' '}
                    <Button color="secondary" onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </>
    );
}

export default EditCar