export type PagingCommand = {
    page:number
    price:string | null,
    make:string | null,
    model:string | null,
    type:string | null,
    mileageFrom:number | null,
    mileageTo:number | null,
    productionYearFrom:number | null,
    productionYearTo:number | null,
}