import React, {useState} from 'react';
import {NewCarDto} from "../../../../types/NewCarDto";
import {Formik} from "formik";
import {Alert, Button, Input} from "reactstrap";
import {postCar} from "../../../../api";
import {navigate} from "hookrouter";
import * as Yup from 'yup'

type Props = {
    car:NewCarDto
}

const NewCarForm:React.FC<Props> = (props) => {

    const [pictures, setPictures] = useState<File[]>([])
    const [desc, setDesc] = useState(props.car.description)

    function onFileChangeHandler(e:any){
        setPictures(e.target.files)
    }

    function handleDescriptionChange(e: any) {
        setDesc(e.target.value)
    }

    function addCar(val: any, bag: any) {
        val = {...val, description:desc};
        bag.setSubmitting(true);
        let formData = new FormData();
        formData.append('car',JSON.stringify(val))
        let i:number;

        for(i=0;i<pictures.length;i++){
            formData.append('file', pictures[i])
        }

        postCar(formData)
            .then((response) => {bag.setSubmitting(false); navigate('/auti/' + response.data)})
            .catch((err) => {bag.setSubmitting(false); alert('Failed to add new car.  ' + err.toString())})
    }

    return(
        <Formik
            initialValues={props.car}
            validationSchema={
                Yup.object().shape({
                    price: Yup.string()
                        .required('Required'),
                    location: Yup.string()
                        .required('Required'),
                    make: Yup.string()
                        .required('Required'),
                    model: Yup.string()
                        .required('Required'),
                    type: Yup.string()
                        .required('Required'),
                    productionYear: Yup.number()
                        .required('Required'),
                    mileage: Yup.number()
                        .required('Required'),
            })}
            onSubmit={addCar}
            render={({
                         values,
                         errors,
                         touched,
                         handleChange,
                         handleBlur,
                         handleSubmit,
                         isSubmitting,
                     }) => (
                <form onSubmit={handleSubmit} className="newCarForm">
                    <ul>
                        <li>
                            <label>Cijena:</label>
                            <input
                                type="number"
                                name="price"
                                placeholder="price"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.price}
                            />
                        </li>{errors.price && touched.price && <Alert color="danger">{errors.price}</Alert>}
                        <li>
                            <label>Lokacija vozila:</label>
                            <input
                                type="text"
                                name="location"
                                placeholder="location"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.location}
                            />
                        </li>{errors.location && touched.location && <Alert color="danger">{errors.location}</Alert>}
                        <li>
                            <label>Marka automobila:</label>
                            <input
                                type="text"
                                name="make"
                                placeholder="make"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.make}
                            />
                        </li>{errors.make && touched.make && <Alert color="danger">{errors.make}</Alert>}
                        <li>
                            <label>Model automobila:</label>
                            <input
                                type="text"
                                name="model"
                                placeholder="model"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.model}
                            />
                        </li>{errors.model && touched.model && <Alert color="danger">{errors.model}</Alert>}
                        <li>
                            <label>Tip automobila:</label>
                            <input
                                type="text"
                                name="type"
                                placeholder="type"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.type}
                            />
                        </li>{errors.type && touched.type && <Alert color="danger">{errors.type}</Alert>}
                        <li>
                            <label>Godina proizvodnje:</label>
                            <input
                                type="number"
                                name="productionYear"
                                placeholder="productionYear"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.productionYear}
                            />
                        </li>{errors.productionYear && touched.productionYear && <Alert color="danger">{errors.productionYear}</Alert>}
                        <li>
                            <label>Godina modela:</label>
                            <input
                                type="number"
                                name="modelYear"
                                placeholder="modelYear"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.modelYear}
                            />
                        </li>{errors.modelYear && touched.modelYear && <Alert color="danger">{errors.modelYear}</Alert>}
                        <li>
                            <label>Prijeđeni kilometri:</label>
                            <input
                                type="number"
                                name="mileage"
                                placeholder="mileage"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.mileage}
                            />
                        </li>{errors.mileage && touched.mileage && <Alert color="danger">{errors.mileage}</Alert>}
                        <li>
                            <label>Motor:</label>
                            <input
                                type="text"
                                name="motor"
                                placeholder="motor"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.motor}
                            />
                        </li>{errors.motor && touched.motor && <Alert color="danger">{errors.motor}</Alert>}
                        <li>
                            <label>Snaga motora:</label>
                            <input
                                type="number"
                                name="motorPower"
                                placeholder="motorPower"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.motorPower}
                            />
                        </li>{errors.motorPower && touched.motorPower && <Alert color="danger">{errors.motorPower}</Alert>}
                        <li>
                            <label>Radni obujam:</label>
                            <input
                                type="number"
                                name="motorCubicCentimeters"
                                placeholder="motorCubicCentimeters"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.motorCubicCentimeters}
                            />
                        </li>{errors.motorCubicCentimeters && touched.motorCubicCentimeters && <Alert color="danger">{errors.motorCubicCentimeters}</Alert>}
                        <li>
                            <label>Mjenjač:</label>
                            <input
                                type="text"
                                name="gearbox"
                                placeholder="gearbox"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.gearbox}
                            />
                        </li>{errors.gearbox && touched.gearbox && <Alert color="danger">{errors.gearbox}</Alert>}
                        <li>
                            <label>Broj stupnjeva:</label>
                            <input
                                type="number"
                                name="numberOfGears"
                                placeholder="numberOfGears"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.numberOfGears}
                            />
                        </li>{errors.numberOfGears && touched.numberOfGears && <Alert color="danger">{errors.numberOfGears}</Alert>}
                        <li>
                            <label>Rabljeno:</label>
                            <input
                                type="checkbox"
                                name="used"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                checked={values.used}
                            />
                        </li>{errors.used && touched.used && <Alert color="danger">{errors.used}</Alert>}
                        <li>
                            <label>Servisna knjiga:</label>
                            <input
                                type="checkbox"
                                name="hasServiceBook"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                checked={values.hasServiceBook}
                            />
                        </li>{errors.hasServiceBook && touched.hasServiceBook && <Alert color="danger">{errors.hasServiceBook}</Alert>}
                        <li>
                            <label>Garažiran:</label>
                            <input
                                type="checkbox"
                                name="garaged"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                checked={values.garaged}
                            />
                        </li>{errors.garaged && touched.garaged && <Alert color="danger">{errors.garaged}</Alert>}
                        <li>
                            <label>images:</label>
                            <input type="file" className="form-control" name="file" multiple onChange={onFileChangeHandler}/>
                        </li>
                        <li>
                            <Input rows={10} type="textarea" name="text" id="exampleText" onChange={handleDescriptionChange} value={desc}/>
                        </li>
                        <li>
                            <Button color="success" type="submit" disabled={isSubmitting}>
                                Add Car
                            </Button>
                        </li>
                    </ul>
                </form>
            )}
        />
    );
}

export default NewCarForm