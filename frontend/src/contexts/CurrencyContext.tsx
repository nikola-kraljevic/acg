import {createContext} from 'react'

const CurrencyContext = createContext<{currency:string, updateCurrency:(arg0:string)=>void}>({currency:"EUR",updateCurrency:()=>{}})

export default CurrencyContext