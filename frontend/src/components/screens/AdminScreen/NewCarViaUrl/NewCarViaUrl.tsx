import React, {useEffect, useState} from "react";
import {Alert, Button, Input, Progress, Spinner} from "reactstrap";
import {addNewCarFromUrl, refreshAllCars} from "../../../../api";
import {navigate} from "hookrouter";

const NewCarViaUrl:React.FC = () => {

    const [disabledHttpsUrl, setDisabledHttpsUrl] = useState(false)
    const [disabledPagedUrl, setDisabledPagedUrl] = useState(false)
    const [httpsUrl, setHttpsUrl] = useState<string>('')
    const [loading, setLoading] = useState<boolean>(false)
    const [addedCars, setAddedCars] = useState<null | number>(null)

    function handleUrlChange(e:any){
        setHttpsUrl(e.target.value)
    }

    
    function handleFetch() {
        setDisabledHttpsUrl(true)
        addNewCarFromUrl(httpsUrl)
            .then((response) => {
                if(response !== "Car already exists"){
                    console.log(" getNewCarDtoFromUrl Success: " + response)
                    setDisabledHttpsUrl(false)
                    navigate("/admin/edit/" + response)
                } else {
                    alert(response)
                    setDisabledHttpsUrl(false)
                }
            })
            .catch((err) => {
                console.log(" getNewCarDtoFromUrl Error: "+err)
                setDisabledHttpsUrl(false)
            })
    }

    function handleCarRefresh() {
        setDisabledPagedUrl(true)
        setLoading(true)
        refreshAllCars()
            .then(response =>
                {setDisabledPagedUrl(false);
                setAddedCars(response);}
            )
            .catch((err)=>{
                setDisabledPagedUrl(false);
            })
    }

    return(
        <>
            <div className="urlInput">
                <div className="one">
                    <div>
                        <div style={{display:"inline-block"}}>
                            <Input
                                style={{width:"400px",marginTop:"5px"}}
                                type="text"
                                placeholder="https://www.njuskalo.hr/auti/..."
                                value={httpsUrl}
                                onChange={handleUrlChange}
                            />
                        </div>
                        <div style={{display:"inline-block"}}>
                            <Button style={{marginBottom:"5px"}} color="success" onClick={handleFetch} disabled={disabledHttpsUrl}>Add Car</Button>
                        </div>
                    </div>
                    <div>
                        <div style={{display:"inline-block"}}>
                            <Button style={{marginBottom:"5px"}} color="success" onClick={handleCarRefresh} disabled={disabledPagedUrl}>Refresh all cars</Button>
                        </div>
                    </div>
                    {loading && <>
                        { disabledPagedUrl && <Spinner color="primary" />}
                        { !disabledPagedUrl &&  <Alert color="success">Done! {addedCars} cars added.</Alert>}
                    </>}
                </div>
                <div className="two">

                </div>
            </div>
        </>
    );
}

export default NewCarViaUrl