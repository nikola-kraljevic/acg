package com.acg.app.ACGApplication.services;

import com.acg.app.ACGApplication.models.Info;

public interface InfoService {

    Info getCarInfoByCarId(String s);
}
