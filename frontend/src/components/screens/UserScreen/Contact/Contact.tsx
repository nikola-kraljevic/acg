import React from 'react'
import MailSender from "./MailSender/MailSender";

const Contact:React.FC = () => {
    return(
        <div className="contactGrid">
            <div className="mailSender"><p className="lead" style={{fontSize:"35px"}}>Kontaktirajte nas putem maila:</p><MailSender/></div>
            <div className="location">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2781.1810313337355!2d16.047978015409146!3d45.80763271848272!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4766786338a18867%3A0x34a537f2263bdab2!2sSobolski%20put%2024%2C%2010000%2C%20Zagreb!5e0!3m2!1sen!2shr!4v1573936014995!5m2!1sen!2shr"
                    height="450" frameBorder="0" style={{border:"0",width:"100%"}} allowFullScreen={false}></iframe>
                <p className="lead">Za sva dodatna pitanja nazovite na broj:</p>
                <p className="lead">099 232 0300</p>
            </div>
        </div>
    )
}

export default Contact
