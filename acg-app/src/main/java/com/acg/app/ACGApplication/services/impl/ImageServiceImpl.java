package com.acg.app.ACGApplication.services.impl;

import com.acg.app.ACGApplication.models.Car;
import com.acg.app.ACGApplication.models.Image;
import com.acg.app.ACGApplication.repositories.CarRepository;
import com.acg.app.ACGApplication.repositories.ImageRepository;
import com.acg.app.ACGApplication.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;

@Service
public class ImageServiceImpl implements ImageService {

    private final ImageRepository imageRepository;
    private final CarRepository carRepository;

    @Autowired
    public ImageServiceImpl(ImageRepository imageRepository, CarRepository carRepository) {
        this.imageRepository = imageRepository;
        this.carRepository = carRepository;
    }

    @Override
    public Image findImageById(String uuid) throws RuntimeException{
        try {
            Optional<Image> image = imageRepository.findById(UUID.fromString(uuid));
            return image.get();
        } catch (Exception e) {
            throw new RuntimeException("No image found");
        }
    }

    @Override
    public List<Image> getCarImagesByCarId(String uuid){
        Optional<Car> car = carRepository.findById(UUID.fromString(uuid));
        List<Image> images = new ArrayList<>();

        if(car.isPresent()){
            images = car.get().getImages();
            Collections.sort(images);
        }
        return images;
    }

    @Override
    @Transactional
    public void deleteImageById(String uuid){
        Image image = findImageById(uuid);
        imageRepository.delete(image);
    }
}
