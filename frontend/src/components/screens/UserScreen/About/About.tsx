import React from 'react'
import abt from "../../../../images/abt.jpg"

const About:React.FC = () => {
    return(
        <>
            <h1>O nama:</h1>
            <h5>Auto centar Grubišić d.o.o. je obiteljska tvrtka koja se bavi uvozom i prodajom rabljenih vozila iz Njemačke. U našem salonu u Zagrebu uvijek je na raspolaganju 50-ak vozila s njemačkog tržišta koje možete pogledati, a ukoliko niste u mogućnosti fizički nas posjetiti, ponudu u svakom trenutku možete pratiti na www.acgrubisic.hr. U svakom trenutku osoblje salona možete kontaktirati putem telefona 099 232 0300 ili putem e-mail-a na acgrubisic@gmail.com, te dobiti sve potrebne informacije u najkraćem roku. Veselimo se Vašem dolasku!</h5>
            <div style={{paddingBottom:"15px"}}><img src={abt} style={{
                "maxWidth": "70%",
                "maxHeight": "70%",
            }}/></div>
        </>
    );
}

export default About
