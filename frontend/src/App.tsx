import React, {useEffect, useState} from 'react'
import './App.css';
import './image-gallery.css'
import Home from "./components/screens/UserScreen/Home/Home";
import Auth from "./components/screens/AdminScreen/Auth/Auth";
import {navigate, useRoutes} from "hookrouter";
import NotFound from "./components/Errors/NotFound/NotFound";
import AdminRoutingHoc from "./components/screens/AdminScreen/AdminRoutingHoc";
import Header from "./components/Navbar/Header";
import Cars from "./components/screens/UserScreen/Cars/Cars";
import AdminCtx from "./contexts/AdminCtx";
import CurrencyContext from "./contexts/CurrencyContext";
import CarInfo from "./components/screens/UserScreen/CarInfo/CarInfo";
import Contact from "./components/screens/UserScreen/Contact/Contact";
import Footer from "./components/Footer/Footer";
import About from "./components/screens/UserScreen/About/About";
import Services from "./components/screens/UserScreen/Services/Services";
import Gallery from "./components/screens/UserScreen/Gallery/Gallery";
import Banner from "./components/Banner/Banner";

require('dotenv').config();

function App() {

  const routes = {
    '/': () => <Home/>,
    '/auth': () => <Auth/>,
    '/admin*': () => <AdminRoutingHoc/>,
    '/auti': () => <Cars/>,
    '/about': () => <About/>,
    '/usluge': () => <Services/>,
    '/galerija': () => <Gallery/>,
    '/auti/:id': ({id} : any) => <CarInfo uuid={id}/>,
    '/kontakt': () => <Contact/>
  };

  const routeResult = useRoutes(routes);

  const [authenticated, setAuthenticated] = useState(false);
  const [currency, setCurrency] = useState("EUR");

  function setAuthCtxToTrue(bool: boolean) {
    if(bool){
      setAuthenticated(bool)
      navigate('/admin')
    }else{
      setAuthenticated(bool)
      navigate('/')
    }
  }

  function updateCurrency(curr:string){
    setCurrency(curr)
  }

  useEffect(() => {
    if(localStorage.getItem('admin')){
      setAuthenticated(true)
    }
  },[authenticated])

  return (
    <div className="App appGrid">
      <AdminCtx.Provider value={{authenticated, updateAuthenticated: setAuthCtxToTrue}}>
        <CurrencyContext.Provider value={{currency, updateCurrency: updateCurrency}}>
          <Banner/>
          <div className="navbarfill navigation" style={{width:"800%", gridColumn:"1/2",float:"left"}}></div>
          <div className="appHeader"><Header/></div>
          <div className="appContent">{routeResult || <NotFound/>}</div>
          <div className="appFooter"><Footer/></div>
        </CurrencyContext.Provider>
      </AdminCtx.Provider>
    </div>
  );
}

export default App;
