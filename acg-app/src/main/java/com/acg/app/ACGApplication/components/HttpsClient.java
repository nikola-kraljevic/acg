package com.acg.app.ACGApplication.components;

import com.acg.app.ACGApplication.commands.CarCommand;
import com.acg.app.ACGApplication.commands.ScrapedData;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketTimeoutException;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

@Component
public class HttpsClient{

    private Long getLongFromStr(String s){
        if(!s.equals("")){return Long.parseLong(s.replaceAll("[^0-9]", ""));}
        return Long.valueOf(0);
    }

    private Integer getIntFromStr(String s){
        if (!s.equals("")){
            return Integer.parseInt(s.replaceAll("[^0-9]", ""));
        }
        return Integer.valueOf(0);
    }

    private Document getHtmlDoc(String url){
        try {

            //Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("77.252.26.71", 57348)); // or whatever your proxy is
            Document doc = null;
            int i = 0;

            while(i < 10){
                try {
                    doc = Jsoup.connect(url)
                            //.proxy(proxy)
                            .get();
                    if(doc.select("div.captcha-mid").size() > 0){
                        throw new RuntimeException("Capatcha >:(");
                    }
                    break;
                } catch (SocketTimeoutException ex){
                    System.out.println("Connection timed out...");
                }

                i++;
            }



            return doc;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ScrapedData scrapeDataFromCarPage(String url){
        Document doc = getHtmlDoc(url);

        ScrapedData data = new ScrapedData();
        CarCommand carCommand = new CarCommand();
        List<String> images = new ArrayList<>();
        data.setCar(carCommand);
        data.setImages(images);

        carCommand.setNjuskalo(url);

        //PRICE
        if(doc.select("span.ClassifiedDetailSummary-priceForeign").size() > 0){
            Element price_li = doc.select("span.ClassifiedDetailSummary-priceForeign").get(0);
            data.getCar().setPrice(getLongFromStr(price_li.text()));
        }


        //car info
        Elements carInfoElements = doc.select("dt.ClassifiedDetailBasicDetails-listTerm");
        for (Element element : carInfoElements){
            String elementValue = element.nextElementSibling().text();
            switch (element.text()){
                case "Lokacija vozila":
                    carCommand.setLocation(elementValue);
                break;
                case "Marka automobila":
                    carCommand.setMake(elementValue);
                break;
                case "Model automobila":
                    carCommand.setModel(elementValue);
                break;
                case "Tip automobila":
                    carCommand.setType(elementValue);
                break;
                case "Godina proizvodnje":
                    carCommand.setProductionYear(getIntFromStr(elementValue));
                break;
                case "Godina modela":
                    carCommand.setModelYear(getIntFromStr(elementValue));
                break;
                case "Prijeđeni kilometri":
                    carCommand.setMileage(getLongFromStr(elementValue));
                break;
                case "Motor":
                    carCommand.setMotor(elementValue);
                break;
                case "Snaga motora":
                    carCommand.setMotorPower(getIntFromStr(elementValue));
                break;
                case "Radni obujam":
                    carCommand.setMotorCubicCentimeters(getIntFromStr(elementValue));
                break;
                case "Mjenjač":
                    carCommand.setGearbox(elementValue);
                break;
                case "Broj stupnjeva":
                    carCommand.setNumberOfGears(getIntFromStr(elementValue));
                break;
                case "Stanje":
                    if(elementValue.equals("rabljeno")){
                        carCommand.setUsed(true);
                    } else {
                        carCommand.setUsed(false);
                    }
                break;
                case "Vlasnik":
                    carCommand.setOwner(elementValue);
                break;
                case "Servisna knjiga":
                    if(elementValue.equals("Da")){
                        carCommand.setHasServiceBook(true);
                    } else {
                        carCommand.setHasServiceBook(false);
                    }
                break;
                case "Garažiran":
                    if(elementValue.equals("Da")){
                        carCommand.setGaraged(true);
                    } else {
                        carCommand.setGaraged(false);
                    }
                break;
            }
        }

        //description
        StringBuilder desc = new StringBuilder();

        if(doc.select("div.ClassifiedDetailDescription-text").size() > 0){
            Element descDiv = doc.select("div.ClassifiedDetailDescription-text").get(0);
            String str = descDiv.html().replaceAll("<br>", "");
            String strWithNewLines = Jsoup.clean(str, "", Whitelist.none(), new Document.OutputSettings().prettyPrint(false));
            desc.append(strWithNewLines);
        }

        desc.append("\n\n");
        desc.append("Dodatne informacije: \n\n");
        Elements lists = doc.select("h3.ClassifiedDetailPropertyGroups-groupTitle");
        for (Element element : lists){
            desc.append(element.text()).append("\n");
            Elements listItems = element.nextElementSibling().child(0).children();
            for(Element item : listItems){
                desc.append("    • ").append(item.text()).append("\n");
            }
            desc.append("\n");
        }
        carCommand.setDescription(desc.toString());

        //images
        Elements thumbs = doc.select("img.MediaGalleryThumbs-image");
        for (Element thumb : thumbs){
            images.add(thumb.attr("data-src").replace("/image-80x60/","/image-w920x690/"));
        }

        return data;
    }

    public List<String> scrapeCarLinksFromUrl(String url){
        Document document = getHtmlDoc(url);

        List<String> links = new ArrayList<>();

        Element ul = document.getElementsByClass("EntityList-items").get(0);
        Elements a = ul.select("a[href^=/auti][name~=^[0-9]*$]");
        int i=0;
        for (i=0;i<a.size();i++){
            links.add("https://www.njuskalo.hr"+a.get(i).attr("href"));
        }

        return links;

    }

    public Integer getNumOfCars(){
        Document document = getHtmlDoc("https://www.njuskalo.hr/trgovina/autocentar-grubisic");
        Element carCountElement = document.select("div.entity-list-meta").get(0);
        return Integer.valueOf(carCountElement.text().replaceAll("[^0-9]", ""));
    }
}
