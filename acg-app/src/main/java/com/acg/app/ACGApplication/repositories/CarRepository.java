package com.acg.app.ACGApplication.repositories;

import com.acg.app.ACGApplication.models.Car;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CarRepository extends JpaRepository<Car, UUID> {

    @Query(value = "select * from CAR where njuskalo=:url", nativeQuery = true)
    Optional<Car> findByUrl(@Param("url") String url);

    @Query(value = "select distinct MAKE from CAR", nativeQuery = true)
    List<String> findDistinctCarMakes();

    @Query(value = "select distinct MODEL from CAR where MAKE =:make", nativeQuery = true)
    List<String> findDistinctModelsOfMake(@Param("make") String make);

    @Query(value = "select distinct TYPE from CAR where MAKE =:make and MODEL =:model", nativeQuery = true)
    List<String> findDistinctTypesByMakeAndModel(@Param("make") String make, @Param("model") String model);

    @Query(value = "select * from CAR",nativeQuery = true)
    Page<Car> findAllByParams(Pageable page);

    @Query(value = "select * from CAR where FEATURED=true",nativeQuery = true)
    Page<Car> findAlFeatured(Pageable page);

    @Query(value = "select * from CAR where " +
            "PRODUCTION_YEAR between :minYear and :maxYear " +
            "and MILEAGE between :minMileage and :maxMileage",
            nativeQuery = true)
    Page<Car> findAllByParams(
            @Param("minYear") Integer minYear,
            @Param("maxYear") Integer maxYear,
            @Param("minMileage") Integer minMileage,
            @Param("maxMileage") Integer maxMileage,
            Pageable page);

    @Query(value = "select * from CAR where " +
            "MAKE =:make " +
            "and PRODUCTION_YEAR between :minYear and :maxYear " +
            "and MILEAGE between :minMileage and :maxMileage",
            nativeQuery = true)
    Page<Car> findAllByParams(
            @Param("minYear") Integer minYear,
            @Param("maxYear") Integer maxYear,
            @Param("minMileage") Integer minMileage,
            @Param("maxMileage") Integer maxMileage,
            @Param("make") String make,
            Pageable page);

    @Query(value = "select * from CAR where " +
            "MAKE =:make " +
            "and MODEL =:model " +
            "and PRODUCTION_YEAR between :minYear and :maxYear " +
            "and MILEAGE between :minMileage and :maxMileage ",
            nativeQuery = true)
    Page<Car> findAllByParams(
            @Param("minYear") Integer minYear,
            @Param("maxYear") Integer maxYear,
            @Param("minMileage") Integer minMileage,
            @Param("maxMileage") Integer maxMileage,
            @Param("make") String make,
            @Param("model") String model,
            Pageable page);

    @Query(value = "select * from CAR where " +
            "MAKE =:make " +
            "and MODEL =:model " +
            "and TYPE =:type " +
            "and PRODUCTION_YEAR between :minYear and :maxYear " +
            "and MILEAGE between :minMileage and :maxMileage",
            nativeQuery = true)
    Page<Car> findAllByParams(
            @Param("minYear") Integer minYear,
            @Param("maxYear") Integer maxYear,
            @Param("minMileage") Integer minMileage,
            @Param("maxMileage") Integer maxMileage,
            @Param("make") String make,
            @Param("model")String model,
            @Param("type") String type,
            Pageable page);

    @Query(value = "select max(PRODUCTION_YEAR) from car", nativeQuery = true)
    Integer getMaxProductionYear();

    @Query(value = "select min(PRODUCTION_YEAR) from car", nativeQuery = true)
    Integer getMinProductionYear();

    @Query(value = "select max(MILEAGE) from car", nativeQuery = true)
    Long getMaxMileage();

    @Query(value = "select min(MILEAGE) from car", nativeQuery = true)
    Long getMinMileage();

    @Query(value = "SELECT * FROM CAR ORDER BY PRICE DESC" +
            " LIMIT 4" , nativeQuery = true)
    List<Car> findExpensiveCars();
}
