package com.acg.app.ACGApplication.controllers;

import com.acg.app.ACGApplication.commands.CarCommand;
import com.acg.app.ACGApplication.commands.PagingCommand;
import com.acg.app.ACGApplication.commands.SortingInfoDto;
import com.acg.app.ACGApplication.models.Car;
import com.acg.app.ACGApplication.services.impl.CarServiceImpl;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/cars")
public class CarController {

    private final CarServiceImpl carService;
    private static final Logger logger = LogManager.getLogger(CarController.class);

    @Autowired
    public CarController(CarServiceImpl carService) {
        this.carService = carService;
    }

    @GetMapping(path = "/")
    public List<Car> findAllCars(){
        return carService.findAllCars();
    }

    @GetMapping(path = "/expensive")
    public List<Car> findExpensive(){
        return carService.findExpensive();
    }

    @GetMapping(path = "/{uuid}")
    public Car findCarById(@PathVariable String uuid){
        return carService.findCarById(UUID.fromString(uuid));
    }

    @GetMapping(path = "/makes")
    public List<String> findAllMakes(){
        return carService.findAllMakes();
    }

    @GetMapping(path = "/{make}/models")
    public List<String> findAllMakeModels(@PathVariable String make){
        return carService.findAllMakeModels(make);
    }

    @GetMapping(path = "/{make}/{model}/types")
    public List<String> findAllTypesByMakeAndModel(@PathVariable String make, @PathVariable String model){
        return carService.findAllTypesByMakeAndModel(make,model);
    }

    @GetMapping(path = "/initial-sorting-data")
    public SortingInfoDto getInitialSortingInfo(){
        return carService.getInitialSortingData();
    }

    @GetMapping(path = "/featured")
    public Page<Car> getFeaturedCars(){
        return carService.getFeaturedCars();
    }

    @PostMapping(path = "/{uuid}")
    public void editCarById(@PathVariable String uuid, @RequestBody CarCommand command){
        logger.info("Post request - editCarById: uuid:" + uuid + ", command:" + command.toString());
        carService.editCarById(uuid,command);
    }

    @PostMapping(path = "/page")
    public Page pageCars(@RequestBody PagingCommand command){
        return carService.pageCars(command);
    }

    @PostMapping(path = "/new")
    public String saveNewCar(@RequestParam("file") MultipartFile[] images, @RequestParam("car") String carCommandJson){
        try {
            logger.info("Post request - saveNewCar");
            return carService.saveNewCar(images,carCommandJson);
        } catch (IOException e) {
            logger.error("Post request - saveNewCar FAILED");
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping(path = "/new/url")
    public String addCarViaUrl(@RequestBody String https_url) throws IOException, InterruptedException {
        String result = decodeUrl(https_url);
        logger.info("Post request - addCarViaUrl, url:" + result);
        return carService.addCarViaUrl(result);
    }

    @PostMapping(path = "/refresh-all")
    public Integer refreshAllCars() throws IOException, InterruptedException {
        logger.info("Post request - refreshAllCars");
        return carService.updateCars();
    }

    @PostMapping(path = "/delete")
    public void deleteCarById(@RequestBody String uuid){
        logger.info("Post request - deleteCarById: uuidid:"+uuid);
        carService.deleteCarById(uuid.substring(0,uuid.length()-1));
    }

    @PostMapping(path = "/sell")
    public void sellCarById(@RequestBody String uuid){
        logger.info("Post request - sellCarById, uuid:"+uuid);
        carService.sellCarById(uuid.substring(0,uuid.length()-1));
    }

    @PostMapping(path = "/unsell")
    public void unsellCarById(@RequestBody String uuid){
        logger.info("Post request - unsellCarById, uuid:"+uuid);
        carService.unsellCarById(uuid.substring(0,uuid.length()-1));
    }

    @PostMapping(path = "/{carId}/profile")
    public void changeProfilePicture(@PathVariable String carId, @RequestBody String imageId){
        logger.info("Post request - changeProfilePicture, imageId:"+imageId);
        carService.changeProfilePicture(carId,imageId.substring(0,imageId.length()-1));
    }

    @PostMapping(path = "/feature")
    public void featureCarById(@RequestBody String uuid){
        logger.info("Post request - featureCarById, uuid:"+uuid);
        carService.featureCarById(uuid.substring(0,uuid.length()-1));
    }

    @PostMapping(path = "/un-feature")
    public void unFeatureCarById(@RequestBody String uuid){
        logger.info("Post request - unFeatureCarById, uuid:"+uuid);
        carService.unFeatureCarById(uuid.substring(0,uuid.length()-1));
    }

    private String decodeUrl(String url) throws UnsupportedEncodingException {
        String result = java.net.URLDecoder.decode(url, StandardCharsets.UTF_8.name());
        return result.substring(0, result.length()-1);
    }

}
