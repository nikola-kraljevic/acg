import React from 'react'
import {IoIosPin,IoIosMail,IoIosPhonePortrait} from "react-icons/io"
import logo from "../../logo.svg";
import {navigate} from "hookrouter";

const Footer:React.FC = () => {
    return(
        <>
            <div className="footer lead">
                <div className="footerElementLeft">
                    <img src={logo} alt="asdf" style={{maxWidth:"130%",maxHeight:"130%"}}/>
                </div>
                <div className="footerNavigation">
                    <b onClick={()=>{navigate("/");window.scrollTo(0, 0)}}> Početna </b> |
                    <b onClick={()=>{navigate("/about");window.scrollTo(0, 0)}}> O nama </b> |
                    <b onClick={()=>{navigate("/usluge");window.scrollTo(0, 0)}}> Dodatne usluge </b> |
                    <b onClick={()=>{navigate("/auti");window.scrollTo(0, 0)}}> Pregled vozila </b> |
                    <b onClick={()=>{navigate("/kontakt");window.scrollTo(0, 0)}}> Kontakt  </b> |
                    <b onClick={()=>{navigate("/galerija");window.scrollTo(0, 0)}}>  Galerija </b>
                </div>
                <ul className="footerInfo">
                    <li>
                        <div className="footerElementRight">
                            <IoIosPin size="1.5em"/> Sobolski put 24
                        </div>
                    </li>
                    <li>
                        <div className="footerElementRight">
                            <IoIosMail size="1.5em"/> acgrubisic@gmail.com
                        </div>
                    </li>
                    <li>
                        <div className="footerElementRight">
                            <IoIosPhonePortrait size="1.5em"/> 099 232 0300
                        </div>
                    </li>
                </ul>
            </div>
        </>
    );
}

export default Footer