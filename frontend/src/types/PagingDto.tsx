import {CarPreviewDto} from "./CarPreviewDto";

export type PagingDto = {
    content: CarPreviewDto[],
    pageable:{
        sort:{
            sorted:boolean,
            unsorted:boolean,
            empty:boolean
        },
        offset:number,
        pageSize:number,
        pageNumber:number,
        paged:boolean,
        unpaged:boolean
    },
    last: boolean,
    totalElements:number,
    totalPages: number,
    number: number,
    size:number,
    sort:{
        sorted:boolean,
        unsorted:boolean,
        empty:boolean
    },
    numberOfElements: number,
    first: boolean,
    empty: boolean
}