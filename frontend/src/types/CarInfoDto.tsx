export type CarInfoDto = {
    uuid: string,
    location: string,
    gearbox:string,
    motor:string,
    mileage:number,
    productionYear:number,
    modelYear:number,
    motorPower:number,
    motorCubicCentimeters:number,
    numberOfGears:number,
    hasServiceBook:boolean,
    description:string,
    isGaraged:boolean,
    used:boolean
}