import React, {useContext, useState} from 'react'
import {
    Button,
    Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavItem,
    NavLink, UncontrolledDropdown
} from 'reactstrap'
import AdminCtx from "../../contexts/AdminCtx";
import logo from '../../logo.svg'
import {navigate} from "hookrouter";
import {IoIosMenu} from "react-icons/io";

const Header:React.FC = () =>{

    const adminCtx = useContext(AdminCtx)

    const [active, setActive] = useState<{
        home:boolean,
        offer:boolean,
        contact:boolean,
    }>({
        home:false,
        offer:false,
        contact:false,
    })

    function handleLogout() {
        adminCtx.updateAuthenticated(false)
        localStorage.removeItem('admin')
    }

    return(
        <>
            <div className={'mobileLogo'} style={{
                height:'100%',
                padding:'6px',
                alignItems: "center",}}>
                <img src={logo} alt="asdf" style={{maxWidth:"100%",maxHeight:"100%"}} onClick={()=>navigate('/')}/>
                <UncontrolledDropdown style={{
                    margin:"0 0 0 auto",
                    display: "block",
                }}>
                    <DropdownToggle>
                        <IoIosMenu size="3em" />
                    </DropdownToggle>
                    <DropdownMenu positionFixed={true}>
                        <DropdownItem onClick={()=>navigate('/')}>Početna</DropdownItem>
                        <DropdownItem onClick={()=>navigate('/auti')}>Pregled vozila</DropdownItem>
                        <DropdownItem onClick={()=>navigate('/about')}>O nama</DropdownItem>
                        <DropdownItem onClick={()=>navigate('/usluge')}>Dodatne usluge</DropdownItem>
                        <DropdownItem onClick={()=>navigate('/kontakt')}>Kontakt</DropdownItem>
                        <DropdownItem onClick={()=>navigate('/galerija')}>Galerija</DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>
            </div>
            <div className="navigation">
                <div className="headerBotun" onClick={()=>navigate('/')}>Početna</div>
                <div className="headerBotun" onClick={()=>navigate('/auti')}>Pregled vozila</div>
                <div className="headerBotun" onClick={()=>navigate('/about')}>O nama</div>
                <div className="headerBotun" onClick={()=>navigate('/usluge')}>Dodatne usluge</div>
                <div className="headerBotun" onClick={()=>navigate('/kontakt')}>Kontakt</div>
                <div className="headerBotun" onClick={()=>navigate('/galerija')}>Galerija</div>
                {adminCtx.authenticated &&
                <>
                    <Button
                        style={{marginRight:"5px"}}
                        color="success"
                        onClick={()=>{navigate('/admin/new/url')}}
                    >
                        Url
                    </Button>
                    <Button
                        style={{marginRight:"5px"}}
                        color="success"
                        onClick={()=>{navigate('/admin/new')}}
                    >
                        New Car
                    </Button>
                    <Button
                        style={{marginRight:"5px"}}
                        color="info"
                        onClick={handleLogout}
                    >
                        Logout
                    </Button>
                </>
                }
            </div>
        </>
    );
}

export default Header