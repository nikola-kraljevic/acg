import React, {useContext, useEffect, useState} from 'react'
import {
    Button, Card, CardBody, Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownToggle, FormGroup,
    Input, Label, ListGroup, ListGroupItem, Spinner,
    UncontrolledCollapse,
    UncontrolledDropdown
} from "reactstrap";
import {
    getAllMakeModels,
    getAllMakes,
    getAllTypesByMakeAndModel,
    getInitialSortingData,
    getPagedCarsByPageNumber
} from "../../../../api";
import {PagingCommand} from "../../../../types/PagingCommand";
import {IoIosArrowRoundDown, IoIosArrowRoundUp} from 'react-icons/io'
import CurrencyContext from "../../../../contexts/CurrencyContext";
import {CarPreviewDto} from "../../../../types/CarPreviewDto";
import Car from "./Car/Car";
import {navigate} from "hookrouter";


const Cars:React.FC = () => {

    const currencyCtx = useContext(CurrencyContext);

    const [cars, setCars] = useState<CarPreviewDto[]>([]);
    const [isLoading, setIsLoading] = useState(false);
    const [hasMore, setHasMore] = useState(true);

    const [pageCommand, setPageCommand] = useState<PagingCommand>({
        page:0,
        price:null,
        make:null,
        model:null,
        type:null,
        mileageFrom:null,
        mileageTo:null,
        productionYearFrom:null,
        productionYearTo:null,
    });
    const [makes, setMakes] = useState<string[] | null>(null);
    const [models, setModels] = useState<string[] | null>(null);
    const [types, setTypes] = useState<string[] | null>(null);
    const [years, setYears] = useState<number[]>([]);
    const [miles, setMiles] = useState<number[]>([]);
    const [toYears, setToYears] = useState<number[]>([]);
    const [toMiles, setToMiles] = useState<number[]>([]);

    window.onscroll = function() {
        if (((window.innerHeight + window.pageYOffset) >= (document.body.offsetHeight-100)) && hasMore && !isLoading) {
            console.log("request more!!!")
            loadMore()
        }
    };

    function loadMore() {
        setIsLoading(true);
        getPagedCarsByPageNumber(pageCommand)
            .then(response=>{
                incrementPage();
                setCars([...cars,...response.content]);
                setHasMore(!response.last);
                setIsLoading(false)
            })
    }

    function incrementPage() {
        setPageCommand({...pageCommand,page:pageCommand.page+1})
    }

    function mileageSearch() {
        setCars([]);
        setHasMore(true);
        setPageCommand({...pageCommand,page:0});
    }

    function handleMileageFromChange(m:number) {
        setCars([]);
        setHasMore(true);
        setPageCommand({...pageCommand, page:0, mileageFrom:m});
    }

    function handleMileageToChange(m:number) {
        setCars([]);
        setHasMore(true);
        setPageCommand({...pageCommand, page:0, mileageTo:m});
    }

    function handlePriceChange(dir:string){
        setCars([]);
        setHasMore(true);
        setPageCommand({...pageCommand, page:0, price:dir});
    }

    function handleMakeClick(make:string){
        setCars([]);
        setHasMore(true);
        setPageCommand({...pageCommand, page:0, make:make, model:null, type:null});
        setModels(null);
        setTypes(null);
    }

    function handleModelClick(model:string){
        setCars([]);
        setHasMore(true);
        setPageCommand({...pageCommand, page:0, model:model, type:null});
        setTypes(null);
    }

    function handleTypeClick(type:string){
        setCars([]);
        setHasMore(true);
        setPageCommand({...pageCommand, page:0, type:type})
    }

    function handleYearToSelect(year:number){
        setCars([]);
        setPageCommand({...pageCommand, page:0, productionYearTo:year});
        setHasMore(true)
    }

    function handleYearFromSelect(year:number){
        setCars([]);
        setPageCommand({...pageCommand, page:0, productionYearFrom:year});
        setHasMore(true)
    }

    //initial info
    useEffect(() => {
        if (pageCommand.mileageFrom === null) {
            if(makes === null) {
                getAllMakes().then(response => setMakes(response));
            }
            getInitialSortingData().then(response => {
                setPageCommand({
                    ...pageCommand,
                    mileageFrom: response.minMileage,
                    mileageTo: response.maxMileage,
                    productionYearFrom: response.minProductionYear,
                    productionYearTo: response.maxProductionYear,
                });
                let arr: number[] = [];
                for (let i = response.minProductionYear; i <= response.maxProductionYear; i++) {
                    arr.push(i);
                }
                setYears(arr);
                setToYears(arr);
                arr = [];
                for (let i = response.minMileage - response.minMileage%10000; i <= response.maxMileage - response.maxMileage%10000 + 10000; i+=10000) {
                    arr.push(i);
                }
                setMiles(arr);
                setToMiles(arr);
            })
        }
    },[]);

    //pageCommand changes
    useEffect(()=>{
        if(pageCommand.mileageFrom){
            if((window.innerHeight===document.body.offsetHeight) && hasMore && !isLoading){
                loadMore()
            }

            if (pageCommand.page === 0 && hasMore && !isLoading){
                loadMore()
            }

            //get all models
            if(pageCommand.make && !models){
                getAllMakeModels(pageCommand.make).then(response => setModels(response))
            }

            //get all types
            if(pageCommand.make && pageCommand.model && !types){
                getAllTypesByMakeAndModel(pageCommand.make,pageCommand.model).then(response => setTypes(response))
            }

            //update years to
            if(pageCommand.productionYearTo && pageCommand.productionYearFrom){
                let arr: number[] = [];
                for (let i = pageCommand.productionYearFrom; i <= years[years.length-1]; i++) {
                    arr.push(i);
                }
                setToYears(arr)
            }

            // todo update mileage to
            // if(pageCommand.mileageFrom && pageCommand.mileageTo){
            //     let arr: number[] = [];
            //     for (let i = pageCommand.mileageFrom - pageCommand.mileageFrom%10000; i <= pageCommand.mileageTo - pageCommand.mileageTo%10000; i+=10000) {
            //         arr.push(i);
            //     }
            //     setToMiles(arr)
            // }
        }

    },[
        cars,
        isLoading,
        hasMore,
        pageCommand.page,
        pageCommand.productionYearFrom,
        pageCommand.productionYearTo,
        pageCommand.mileageTo,
        pageCommand.mileageFrom,
        pageCommand.make,
        pageCommand.model,
    ]);



    return(
        <>
    <div className="carPagingWrapper">
        <div className="pagedCars">
            {cars.map(car=>
                <>
                    <Car
                        car={car}
                        func={()=>{navigate('/auti/'+car.id)}}
                    />
                </>
            )}

        </div>
        <div className="sortingPanel">
            <div>
                <>
                    Valuta:
                    <UncontrolledDropdown className={"searchThing"}>
                        <DropdownToggle caret>
                            {currencyCtx.currency}
                        </DropdownToggle>
                        <DropdownMenu>
                            <DropdownItem onClick={()=>currencyCtx.updateCurrency("HRK")}>HRK</DropdownItem>
                            <DropdownItem onClick={()=>currencyCtx.updateCurrency("EUR")}>EUR</DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </>

                <>
                    Cijena:
                    <UncontrolledDropdown className={"searchThing"}>
                        <DropdownToggle caret>
                            {pageCommand.price === "asc" && <>Manja prema većoj <IoIosArrowRoundUp size="1em"/></>}
                            {pageCommand.price === "desc" && <>Veća prema manjoj <IoIosArrowRoundDown size="1em"/></>}
                        </DropdownToggle>
                        <DropdownMenu>
                            <DropdownItem onClick={() => handlePriceChange("asc")}>Manja prema većoj <IoIosArrowRoundUp size="1em"/></DropdownItem>
                            <DropdownItem onClick={() => handlePriceChange("desc")}>Veća prema manjoj <IoIosArrowRoundDown size="1em"/></DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </>

                <>
                    Kilometara:
                    <div style={{display:"flex", width:"100%"}}  className={"searchThing"}>
                        <UncontrolledDropdown style={{width:"49%",marginRight:"5px"}}>
                            <DropdownToggle caret>
                                {pageCommand.mileageFrom && pageCommand.mileageFrom}
                            </DropdownToggle>
                            <DropdownMenu>
                                {miles.map(m=>
                                    <DropdownItem onClick={()=>{handleMileageFromChange(m)}}>{m}</DropdownItem>
                                )}
                            </DropdownMenu>
                        </UncontrolledDropdown>
                        <UncontrolledDropdown style={{width:"49%"}}>
                            <DropdownToggle caret>
                                {pageCommand.mileageTo && pageCommand.mileageTo}
                            </DropdownToggle>
                            <DropdownMenu>
                                {toMiles.map(m=>
                                    <DropdownItem onClick={()=>{handleMileageToChange(m)}}>{m}</DropdownItem>
                                )}
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </div>
                </>

                <>
                    Godiste:
                    <div style={{display:"flex", width:"100%"}}  className={"searchThing"}>
                        <UncontrolledDropdown style={{width:"49%",marginRight:"5px"}}>
                            <DropdownToggle caret>
                                {pageCommand.productionYearFrom && pageCommand.productionYearFrom}
                            </DropdownToggle>
                            <DropdownMenu>
                                {years.map(y=>
                                    <DropdownItem onClick={()=>{handleYearFromSelect(y)}}>{y}</DropdownItem>
                                )}
                            </DropdownMenu>
                        </UncontrolledDropdown>
                        <UncontrolledDropdown style={{width:"49%"}}>
                            <DropdownToggle caret>
                                {pageCommand.productionYearTo && pageCommand.productionYearTo}
                            </DropdownToggle>
                            <DropdownMenu>
                                {toYears.map(y=>
                                    <DropdownItem onClick={()=>{handleYearToSelect(y)}}>{y}</DropdownItem>
                                )}
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </div>
                </>

                <>
                    Marka:
                    <UncontrolledDropdown  className={"searchThing"}>
                        <DropdownToggle caret>
                            {pageCommand.make && pageCommand.make}
                        </DropdownToggle>
                        <DropdownMenu>
                            {makes && makes.map(make =>
                                <DropdownItem
                                    onClick={() => handleMakeClick(make)}>
                                    {make}
                                </DropdownItem>
                            )}
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </>

                <>
                    Model:
                    <UncontrolledDropdown  className={"searchThing"}>
                        <DropdownToggle caret disabled={!pageCommand.make}>
                            {pageCommand.model && pageCommand.model}
                        </DropdownToggle>
                        <DropdownMenu>
                            {models && models.map(model =>
                                <DropdownItem
                                    onClick={() => handleModelClick(model)}>
                                    {model}
                                </DropdownItem>
                            )}
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </>

                <>
                    Tip:
                    <UncontrolledDropdown  className={"searchThing"}>
                        <DropdownToggle caret disabled={!pageCommand.model}>
                            {pageCommand.type && pageCommand.type}
                        </DropdownToggle>
                        <DropdownMenu>
                            {types && types.map(type =>
                                <DropdownItem
                                    onClick={() => handleTypeClick(type)}
                                >{type}</DropdownItem>
                            )}
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </>
                <div style={{width:"100%",display:"flex",alignItems:"center",justifyContent:"center"}}>
                    <div style={{display:"inline-block",margin:"0 auto"}}>
                        <Button color={"success"} onClick={()=>mileageSearch()} style={{marginLeft:"auto"}}>Primjeni</Button>
                    </div>
                </div>
            </div>
            {/*<ListGroup style={{width:"100%"}}>*/}
            {/*    <ListGroupItem id="currencyToggle" color="info">*/}
            {/*        {currencyCtx.currency}*/}
            {/*    </ListGroupItem>*/}
            {/*    <UncontrolledCollapse toggler="#currencyToggle">*/}
            {/*        <Card>*/}
            {/*            <ListGroupItem id="currencyToggle" action onClick={()=>currencyCtx.updateCurrency("HRK")}>*/}
            {/*                HRK*/}
            {/*            </ListGroupItem>*/}
            {/*            <ListGroupItem id="currencyToggle" action onClick={()=>currencyCtx.updateCurrency("EUR")}>*/}
            {/*                EUR*/}
            {/*            </ListGroupItem>*/}
            {/*        </Card>*/}
            {/*    </UncontrolledCollapse>*/}
            {/*</ListGroup>*/}
            {/*<ListGroup style={{width:"100%"}}>*/}
            {/*    <ListGroupItem id="priceToggle" color="info">*/}
            {/*        Cijena{pageCommand.price && <>(*/}
            {/*        {pageCommand.price==="asc"?(<>rastuća</>):(<>padajuća</>)}*/}
            {/*    )</>}*/}
            {/*    </ListGroupItem>*/}
            {/*    <UncontrolledCollapse toggler="#priceToggle">*/}
            {/*        <Card>*/}
            {/*            <ListGroupItem id="priceToggle" action onClick={() => handlePriceChange("asc")}>*/}
            {/*                Manja prema većoj <IoIosArrowRoundUp size="1em"/>*/}
            {/*            </ListGroupItem>*/}
            {/*            <ListGroupItem id="priceToggle" action onClick={() => handlePriceChange("desc")}>*/}
            {/*                Veća prema manjoj <IoIosArrowRoundDown size="1em"/>*/}
            {/*            </ListGroupItem>*/}
            {/*        </Card>*/}
            {/*    </UncontrolledCollapse>*/}
            {/*</ListGroup>*/}
            {/*<ListGroup>*/}
            {/*    <ListGroupItem id="kmToggle" color="info">*/}
            {/*        Kilometara{pageCommand.mileageTo && pageCommand.mileageFrom && <>({Math.round(pageCommand.mileageFrom/1000)}k-{Math.round(pageCommand.mileageTo/1000)}k)</>}*/}
            {/*    </ListGroupItem>*/}
            {/*    <UncontrolledCollapse toggler="#kmToggle">*/}
            {/*        <ListGroupItem>Od*/}
            {/*            <Input*/}
            {/*                value={pageCommand.mileageFrom || undefined}*/}
            {/*                onChange={handleMileageFromChange}*/}
            {/*            />*/}
            {/*        </ListGroupItem>*/}
            {/*        <ListGroupItem>Do*/}
            {/*            <Input*/}
            {/*                value={pageCommand.mileageTo || undefined}*/}
            {/*                onChange={handleMileageToChange}*/}
            {/*            />*/}
            {/*        </ListGroupItem>*/}
            {/*        <ListGroupItem>*/}
            {/*            <Button color={"success"} onClick={()=>mileageSearch()} id="kmToggle">Primjeni</Button>*/}
            {/*        </ListGroupItem>*/}
            {/*    </UncontrolledCollapse>*/}
            {/*</ListGroup>*/}
            {/*<ListGroup>*/}
            {/*    <ListGroupItem id="godisteToggler" color="info">*/}
            {/*        Godište{pageCommand.productionYearFrom && pageCommand.productionYearTo && <>({pageCommand.productionYearFrom}.-{pageCommand.productionYearTo}.)</>}*/}
            {/*    </ListGroupItem>*/}
            {/*    <UncontrolledCollapse toggler="#godisteToggler">*/}
            {/*        <ListGroupItem>Od*/}
            {/*            <Input*/}
            {/*                value={pageCommand.productionYearFrom || undefined}*/}
            {/*                onChange={handleYearFromChange}*/}
            {/*            />*/}
            {/*        </ListGroupItem>*/}
            {/*        <ListGroupItem>Do*/}
            {/*            <Input*/}
            {/*                value={pageCommand.productionYearTo || undefined}*/}
            {/*                onChange={handleYearToChange}*/}
            {/*            />*/}
            {/*        </ListGroupItem>*/}
            {/*        <ListGroupItem>*/}
            {/*            <Button color={"success"} onClick={()=>mileageSearch()} id="godisteToggler">Primjeni</Button>*/}
            {/*        </ListGroupItem>*/}
            {/*    </UncontrolledCollapse>*/}
            {/*</ListGroup>*/}
            {/*<ListGroup style={{width:"100%"}}>*/}
            {/*    <ListGroupItem id="makeToggle" color="info">*/}
            {/*        {!pageCommand.make && "Marka"}*/}
            {/*        {pageCommand.make}*/}
            {/*    </ListGroupItem>*/}
            {/*    <UncontrolledCollapse toggler="#makeToggle">*/}
            {/*        {makes && makes.map(make =>*/}
            {/*            <ListGroupItem*/}
            {/*                id="makeToggle"*/}
            {/*                onClick={() => handleMakeClick(make)}*/}
            {/*            >{make}</ListGroupItem>*/}
            {/*        )}*/}
            {/*    </UncontrolledCollapse>*/}
            {/*</ListGroup>*/}
            {/*{pageCommand.make && <ListGroup>*/}
            {/*    <ListGroupItem id="modelToggle" color="info">*/}
            {/*        {!pageCommand.model && "Model"}*/}
            {/*        {pageCommand.model}*/}
            {/*    </ListGroupItem>*/}
            {/*    <UncontrolledCollapse toggler="#modelToggle">*/}
            {/*        {models && models.map(model =>*/}
            {/*            <ListGroupItem*/}
            {/*                id="modelToggle"*/}
            {/*                onClick={() => handleModelClick(model)}*/}
            {/*            >{model}</ListGroupItem>*/}
            {/*        )}*/}
            {/*    </UncontrolledCollapse>*/}
            {/*</ListGroup>}*/}
            {/*{pageCommand.model && <ListGroup>*/}
            {/*    <ListGroupItem id="typeToggle" color="info">*/}
            {/*        {!pageCommand.type && "Tip"}*/}
            {/*        {pageCommand.type}*/}
            {/*    </ListGroupItem>*/}
            {/*    <UncontrolledCollapse toggler="#typeToggle">*/}
            {/*        {types && types.map(type =>*/}
            {/*            <ListGroupItem*/}
            {/*                id="typeToggle"*/}
            {/*                onClick={() => handleTypeClick(type)}*/}
            {/*            >{type}</ListGroupItem>*/}
            {/*        )}*/}
            {/*    </UncontrolledCollapse>*/}
            {/*</ListGroup>}*/}
        </div>
    </div>
            {isLoading && <Spinner style={{ width: '3rem', height: '3rem', margin:"auto"}} />}
            </>
    );
};

export default Cars