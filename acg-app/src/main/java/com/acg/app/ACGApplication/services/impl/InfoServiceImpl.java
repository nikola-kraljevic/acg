package com.acg.app.ACGApplication.services.impl;

import com.acg.app.ACGApplication.models.Car;
import com.acg.app.ACGApplication.models.Info;
import com.acg.app.ACGApplication.repositories.CarRepository;
import com.acg.app.ACGApplication.repositories.InfoRepository;
import com.acg.app.ACGApplication.services.InfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class InfoServiceImpl implements InfoService {

    private final InfoRepository infoRepository;
    private final CarRepository carRepository;

    @Autowired
    public InfoServiceImpl(InfoRepository infoRepository, CarRepository carRepository) {
        this.infoRepository = infoRepository;
        this.carRepository = carRepository;
    }

    @Override
    public Info getCarInfoByCarId(String s){
        Car car = carRepository.findById(UUID.fromString(s)).get();

        return car.getInfo();
    }
}
