package com.acg.app.ACGApplication.services;

import com.acg.app.ACGApplication.commands.CarCommand;
import com.acg.app.ACGApplication.commands.PagingCommand;
import com.acg.app.ACGApplication.commands.SortingInfoDto;
import com.acg.app.ACGApplication.models.Car;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CarService {

    List<Car> findAllCars();

    List<Car> findExpensive();

    Car findCarById(UUID uuid);

    Optional<Car> findCarByUrl(String url);

    Page<Car> pageCars(PagingCommand command);

    Page<Car> getFeaturedCars();

    void saveImageFileToImageList(String uuid, MultipartFile file);

    void editCarById(String uuid,CarCommand command);

    String saveNewCar(MultipartFile[] images, String carCommandJson) throws IOException;

    String addCarViaUrl(String https_url) throws IOException, InterruptedException;

    void deleteCarById(String uuid);

    void sellCarById(String uuid);

    void unsellCarById(String uuid);

    void featureCarById(String uuid);

    void unFeatureCarById(String uuid);

    void changeProfilePicture(String carId, String imageId);

    List<String> findAllMakes();

    List<String> findAllMakeModels(String make);

    List<String> findAllTypesByMakeAndModel(String make, String model);

    Integer updateCars() throws IOException, InterruptedException;

    SortingInfoDto getInitialSortingData();
}
