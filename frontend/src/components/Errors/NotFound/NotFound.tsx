import React from 'react'

const NotFound:React.FC = () => {
    return(
        <>
            <div style={{minHeight:"80vh"}}>
                404.
            </div>
        </>
    );
}

export default NotFound