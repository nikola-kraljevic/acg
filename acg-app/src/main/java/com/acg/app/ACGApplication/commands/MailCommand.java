package com.acg.app.ACGApplication.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MailCommand {
    private String name;
    private String surname;
    private String phone;
    private String subject;
    private String text;
    private String email;
}
