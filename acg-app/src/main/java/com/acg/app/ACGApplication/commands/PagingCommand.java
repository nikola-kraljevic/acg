package com.acg.app.ACGApplication.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PagingCommand {
    private Integer page;
    private String price;
    private String make;
    private String model;
    private String type;
    private Integer mileageFrom;
    private Integer mileageTo;
    private Integer productionYearFrom;
    private Integer productionYearTo;
}
