package com.acg.app.ACGApplication.controllers;

import com.acg.app.ACGApplication.models.Info;
import com.acg.app.ACGApplication.services.InfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/info")
public class InfoController {

    private final InfoService infoService;

    @Autowired
    public InfoController(InfoService infoService) {
        this.infoService = infoService;
    }

    @GetMapping(path = "/{carId}")
    public Info findCarInfo(@PathVariable String carId){
        return infoService.getCarInfoByCarId(carId);
    }
}
